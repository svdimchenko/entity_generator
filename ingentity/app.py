import yaml
import os
import logging

from ingentity import data_platform_entity

logger = logging.getLogger(__name__)


def read_objects(file):
    objects = []
    with open(file, 'r') as f:
        for idx, val in enumerate(f):
            objects.append(tuple(val.strip().split(',')))
    return objects[1:]  # return objects without schema


def create_directory(path):
    try:
        os.makedirs(path, exist_ok=True)
    except OSError:
        logger.exception("Creation of the directory %s failed" % path, exc_info=True)
    else:
        logger.info("Successfully created the directory %s " % path)


def write_entity(entity, target_directory):
    path = f"{target_directory}/{entity.name}"
    create_directory(path)
    with open(f"{path}/{entity.name}.yml", "w") as f:
        yaml.dump(entity, f)


def run(file, target_directory):
    objects = read_objects(file)
    logger.info("Start objects processing")
    for obj in objects:
        logger.info(f"Start {obj}")
        entity = data_platform_entity.DataPlatformEntity(*obj)
        write_entity(entity, target_directory)
        logger.info(f"Finish {obj}")