mssql_cred_path: str = "/home/svdimchenko/Documents/repos/secret/credentials/mssql_server_credentials_prod.json"

transform_options = {
    "add_table_name": False,
    "decimal_cast_type": "to_numeric",  # possible options ['to_bigint', 'to_numeric']
    "switch_offset": "+00:00",
}

bq_options = {"project_id": "antifraud-production", "dataset_id": "dwh_staging"}

kafka_properties = {
    "kafka_connect_url": "http://kafka-connect.bc-prod.cloud:8083/connectors",
    "schemaRegistryLocation": "http://172.24.0.73:8081",
    "topics": ""
}
