import pymssql
import json


def get_credentials(path_to_credentials):
    with open(path_to_credentials, "r") as f:
        return json.load(f)


def read_table_schema(table, creds_path):
    stmt = f"""
            SELECT COLUMN_NAME, DATA_TYPE, NUMERIC_PRECISION, NUMERIC_SCALE
            FROM INFORMATION_SCHEMA.COLUMNS
            WHERE TABLE_NAME = '{table}'
            """
    conn = pymssql.connect(**get_credentials(creds_path))
    cursor = conn.cursor()
    cursor.execute(stmt)
    schema = cursor.fetchall()
    cursor.close()
    return schema
