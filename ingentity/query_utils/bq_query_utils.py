from ingentity import config

if config.transform_options["decimal_cast_type"] == "to_bigint":
    decimal_target = "INT64"
elif config.transform_options["decimal_cast_type"] == "to_numeric":
    decimal_target = "NUMERIC"

mssql_bq_types_mapping = {
    "decimal": decimal_target,
    "bigint": "INT64",
    "int": "INT64",
    "char": "STRING",
    "datetimeoffset": "TIMESTAMP",
    "timestamp": "TIMESTAMP",
    "bit": "INT64",
    "nvarchar": "STRING",
    "varchar": "STRING",
    "smallint": "INT64",
    "time": "TIMESTAMP",
    "datetime": "TIMESTAMP",
    "date": "TIMESTAMP",
    "smalldatetime": "TIMESTAMP",
}


def bq_statement(table_name, field_name, field_type):
    prefix = table_name if config.transform_options["add_table_name"] else ""
    return f"{prefix}{field_name} {mssql_bq_types_mapping[field_type]}"
