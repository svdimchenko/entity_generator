from ingentity import config


def fix_keywords(field):
    keywords_list = ["Order", "User"]
    return f"[{field}]" if field in keywords_list else field


def mssql_field_statement(table_name, table_alias, field_name, field_type, precision, scale):
    field_prefix = table_name if config.transform_options["add_table_name"] else ""
    source_field = f"{table_alias}.{fix_keywords(field_name)}"
    target_field = f"{field_prefix}{field_name}"

    def _decimal_statement():
        if config.transform_options["decimal_cast_type"] == "to_bigint":
            suffix = f"_{scale}S"
            scaled_exp = pow(10, scale) if scale else None
            return f"CAST({source_field} * {scaled_exp} AS BIGINT) AS {target_field}{suffix}"
        elif config.transform_options["decimal_cast_type"] == "to_numeric":
            return f"CAST({source_field} AS NUMERIC({precision},{scale})) AS {target_field}"

    def _datetime_statement():
        offset = config.transform_options["switch_offset"]
        return f"CAST(SWITCHOFFSET({source_field}, {offset}) AS DATETIME2) AS {target_field}"

    def _int_statement():
        return f"CAST({source_field} AS INT) AS {target_field}"

    def _no_transform_statement():
        return f"{source_field} AS {target_field}"

    types_casting = {
        "decimal": _decimal_statement(),
        "datetimeoffset": _datetime_statement(),
        "datetime": _datetime_statement(),
        "smalldatetime": _datetime_statement(),
        "date": _datetime_statement(),
        "timestamp": _datetime_statement(),
        "time": _datetime_statement(),
        "bit": _int_statement(),
        "smallint": _int_statement(),
        "int": _no_transform_statement(),
        "bigint": _no_transform_statement(),
        "char": _no_transform_statement(),
        "nvarchar": _no_transform_statement(),
        "varchar": _no_transform_statement(),
    }

    return types_casting[field_type]
