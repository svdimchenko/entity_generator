from ingentity import utils, config
from ingentity.query_utils import mssql_query_utils, bq_query_utils


class DataPlatformEntity:
    def __init__(self, name, alias, timestamp_field, description):
        self.name = name
        self.alias = alias
        self.timestamp_field = timestamp_field
        self.description = description
        self.kafka_properties = config.kafka_properties

        self.mssql_properties = self.get_mssql_properties()  # dict with fields schema, query, rowcount_query
        self.bq_properties = self.get_bq_properties()  # dict with fields schema, query

    def get_mssql_properties(self):

        raw_schema = utils.read_table_schema(self.name, config.mssql_cred_path)
        schema = {"table": self.name, "fields": []}
        schema_keys = ["name", "type", "precision", "scale"]

        query = "SELECT "
        for field in raw_schema:
            field_query = mssql_query_utils.mssql_field_statement(self.name, self.alias, *field)
            query += field_query + ", "
            schema["fields"].append(dict(zip(schema_keys, field)))

        query = query[:-2] + " "  # replace last comma symbol
        query += f"FROM {self.name} {self.alias} "
        query_where_part = f"WHERE {self.timestamp_field} = '{{ ds }}'" if self.timestamp_field else ""
        query += query_where_part

        rowcount_query = f"SELECT COUNT(*) FROM {self.name} {query_where_part}"
        return {"mssql_schema": schema, "mssql_read_query": query, "mssql_rowcount_query": rowcount_query}

    def get_bq_properties(self):
        project_id = config.bq_options["project_id"]
        dataset_id = config.bq_options["dataset_id"]
        table_id = self.name
        schema = {"project_id": project_id, "dataset_id": dataset_id, "table_id": table_id, "fields": []}
        query = f"CREATE TABLE IF NOT EXISTS `{project_id}.{dataset_id}.{table_id}` ( "
        for field in self.mssql_properties["mssql_schema"]["fields"]:
            field_query = bq_query_utils.bq_statement(table_id, field["name"], field["type"])
            query += field_query + ", "
            schema["fields"].append({"name": field_query.split(" ")[0], "type": field_query.split(" ")[1]})
        query = query[:-2] + " "  # replace last comma symbol
        query_partition_part = f"PARTITION BY DATE({self.timestamp_field}) " if self.timestamp_field else ""
        query += query_partition_part
        query += f"""OPTIONS (description = "{table_id} table {query_partition_part}")"""
        return {"bq_schema": schema, "bq_create_query": query}
