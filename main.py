import argparse

from ingentity import app

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Ingest Entity Generator')
    parser.add_argument('--file', help='file with objects list')
    parser.add_argument('--target_directory', help='target directory for descriptors')
    args = parser.parse_args()
    app.run(args.file, args.target_directory)
